FROM php:7-apache

RUN apt-get -y update \
 && apt-get -y install apt-utils libldap2-dev libpng-dev libxml2-dev libxslt1-dev libzip-dev libfreetype6-dev \
    libjpeg62-turbo-dev tar curl imagemagick mariadb-client net-tools unzip iputils-ping \
 && apt-get -y autoremove \
 && apt-get -y clean \
 && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
 && docker-php-ext-install calendar gd ldap mysqli pdo_mysql soap sockets xsl zip \
 && a2enmod headers allowmethods

